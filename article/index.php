<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <title>Zespół Szkół AgroTechnicznych w Ropczycach Esport</title>
    <meta name="keywords" content="ezsat, zsat-ropczyce, esport, league of legends, Counter-Strike: Global Offensive, zsat" />
    <link href="../css/style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
    <link rel="Shortcut icon" href="../img/ico.ico" />

    <?php
        include('../admin/dbData.php');
        $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);

        $pagenum = $_GET['page'];
        $numberOfArt = mysqli_fetch_assoc(mysqli_query($conn,"SELECT COUNT(id) as total FROM article"));

        $querymain = "SELECT * FROM article ORDER BY ID DESC LIMIT 1";
        $main = mysqli_query($conn,$querymain);
        $rowmain = mysqli_fetch_array($main);

        $start=(($pagenum-1)*9);
        $query = "SELECT * FROM article ORDER BY ID DESC LIMIT 9 OFFSET ".$start;
        $result = mysqli_query($conn,$query);
    ?>
</head>

<body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="../img/small_logo.png" class="img-responsive" style="height: 30px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <?php
            include("../navbar.php");
        ?>
    </nav>

    <div id="all_article">
      <div class="container-fluid" id="article">
        <h1><b>Artykuły</b></h1>
        <!-- Row -->
        <div class="row my-4">
          <div class="someId1">
            <img id="mainImage" src="" alt="">
          </div>
          <!-- /.col-lg-8 -->
          <div class="col-lg-4">
            <div id="text">
              <h1 id="title">Temat</h1>
                <p id="content">Opis jest super Kocham mój zespół w którym jestem nikim oto moja historia. Opis jest super Kocham mój zespół w którym jestem nikim oto moja historia
                . Opis jest super Kocham mój zespół w którym jestem nikim oto moja historia. Opis jest super Kocham mój zespół w którym jestem nikim oto moja historia...</p>
                <a id="link" href="" style="float: right;">Czytaj dalej</a>
            </div>
          </div>
          <!-- /.col-md-4 -->
        </div>
        <!-- /.row -->
        <h1><b>Pozostałe Artykuły</b></h1>

        <div class="row gallery">
        <?php
            while($row = mysqli_fetch_array($result))
            {
                echo '<div class="someId2" style="padding-top: 18px;padding-bottom: 20px;">';
                echo '<a id="sized" class="article-box" href="article.php?id='.$row['id'].'">';
                echo '<img id="sized" src="'.$row['image'].'" alt="">';
                echo ' <div class="article-box-caption">
                    <div class="article-box-caption-content">
                    <div class="post-description text-faded">'.substr($row['content'],0,50).
                    '...</div></div></div></a>';
                echo '<div class="post-theme">'.$row['title'].'</div></div>';
            }
        ?>
        </div>
      </div>
        <div style="text-align: center; margin-top: 2%;">
          <a id="before" href=""><button type="button" class="btn btn-primary btn-lg" style="background-color:#a9cc17; border-color: #a9cc17;">Poprzednia strona</button></a>
          <a id="next" href=""><button type="button" class="btn btn-primary btn-lg" style="background-color:#a9cc17; border-color: #a9cc17;">Następna strona</button></a>
        </div>
      </div>
      <!-- /.container -->

    <!-- Start footer -->
    <footer>
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="http://jakubbachorz.com/">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
      </div>
    </footer>

    <!-- Bootstrap and JavaScript -->
    <script>
        document.getElementById("mainImage").src= <?php echo "'".$rowmain['image']."'" ?>;
        document.getElementById("title").innerHTML= <?php echo "'".$rowmain['title']."'" ?>;
        document.getElementById("content").innerHTML= <?php echo "'".substr($rowmain['content'],0,300)."...'" ?>;
        document.getElementById("link").href= <?php echo "'article.php?id=".$rowmain['id']."'" ?>;
    </script>
    <script>
        var page=<?php echo $pagenum; ?>;
        var numberOfPages=<?php echo $numberOfArt['total']; ?>;
        numberOfPages=Math.ceil(numberOfPages/9);
        if(page==1)
        {
            document.getElementById("before").style.display="none";
        }
        if(page==numberOfPages)
        {
            document.getElementById("next").style.display="none";
        }
        document.getElementById("before").href= "?page="+(page-1);
        document.getElementById("next").href= "?page="+(page+1);
    </script>
    <script src="../js/jquery/jquery.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>
    <script src="../js/script.js"></script>
    <script type="text/javascript">
    </script>
</body>

</html>
