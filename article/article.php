<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <title>Zespół Szkół AgroTechnicznych w Ropczycach Esport</title>
    <meta name="keywords" content="ezsat, zsat-ropczyce, esport, league of legends, Counter-Strike: Global Offensive, zsat" />
    <link href="../css/style.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
    <link rel="Shortcut icon" href="../img/ico.ico" />

        <?php
            global $resultSide;
            include('../admin/dbData.php');
            $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
            $query = "SELECT * FROM article WHERE id=".$_GET['id'];
            $result = mysqli_query($conn,$query);
            global $row;
            $row = mysqli_fetch_array($result);
            echo '<title>'.$row['title'].'</title>';
            $querySide = "SELECT * FROM article ORDER BY ID DESC LIMIT 6";
            $resultSide = mysqli_query($conn,$querySide);
        ?>
</head>

<body id="page-top">

    <style type="text/css">
        body{
            color: #a9cc17;
        }
        .container-fluid{
          margin-top: 4.5%;
        }
        .col-xs-6{
          font-size: 19px;
        }
        .col-xs-6 li{
          margin-left: 10px;
        }
        .lead{
          font-size: 12px;
        }
        #post_text_head{
          margin-left: 5%;
          top: 48%;
          position: absolute;
          color: #fff;
        }
        #text_icon{
          position: absolute;
          color: #fff;
          margin-top: -12%;
        }
        #lastpost img{
          margin-bottom: 4%;
        }
        #lastpost h3{
          font-size: 16px;
        }
        .img-fluid{
          width: 100%;
          height: 55%;
          object-fit: cover;
          opacity: 0.6;
        }
        footer{
          margin-top: 100px;
        }
        @media (max-width:480px){
        .img-fluid{
          width: 100%;
          height: 35%;
          object-fit: cover;
          opacity: 0.4;
        }
        #post_text_head{
          position: absolute;
          top: 26%;
          color: #fff;
          font-size: 25px;
        }
        #lastpost img{
          margin-bottom: 0.5%;
        }
        .post{
          margin-top: 8vh;
          font-size: 16px;
        }
        .list_post{
          margin-top: 40vh;
        }
        footer{
          margin-top: 10px;
        }
        }
        @media (max-width:360px){
        .img-fluid{
          width: 100%;
          height: 35%;
          object-fit: cover;
        }
        .container-fluid{
          margin-top: 1vh;
        }
        .list_post{
          margin-top: 57vh;
        }
        footer{
          margin-top: 10px;
        }
        }
    </style>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="../img/small_logo.png" class="img-responsive" style="height: 30px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <?php
            include("../navbar.php");
        ?>
    </nav>

    <div class="container-fluid">
      <div class="row">

        <div class="col-xs-6 col-md-10 post" style="height: 850px;">
          <img id="post_image" class="img-fluid" alt="Responsive image" src="imgpost/2.jpg" alt="" >
          <h1 id="post_text_head">Jak zostałem Front-end developerem</h1>
          <p class="lead">Dodano: <span id="post_date"></span><br>Autor: <span id="post_author"></span></p>
          <!-- Post Content -->
            <div id="text_post"></div>
        </div>

        <div class="col-xs-6 col-md-2 list_post">
          <div id="lastpost">
            <?php
                while($rowSide = mysqli_fetch_array($resultSide))
                {
                    echo '<a href="article.php?id='.$rowSide['id'].'">';
                    echo '<img class="img-fluid rounded" src="'.$rowSide['image'].'" alt="">';
                    echo '<h3 id="text_icon">'.substr($rowSide['title'],0,30).' ...</h3></a>';
                }
            ?>
           </div>
         </div>
       </div>
     </div>
    <!-- Start footer -->
    <footer>
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="http://jakubbachorz.com/">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
       </div>
     </footer>

    <!-- Bootstrap and JavaScript -->
    <script>
        document.getElementById("post_text_head").innerHTML = <?php echo "'".$row['title']."'"; ?>;
        document.getElementById("post_image").src = <?php echo "'".$row['image']."'"; ?>;
        document.getElementById("text_post").innerHTML = <?php echo "'".$row['content']."'"; ?>;
        document.getElementById("post_date").innerHTML = <?php echo "'".$row['date']."'"; ?>;
        document.getElementById("post_author").innerHTML = <?php echo "'".$row['author']."'"; ?>;
    </script>
    <script src="../js/jquery/jquery.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>
    <script src="../js/script.js"></script>
    <script type="text/javascript">
    </script>
    <?php
        mysqli_close($conn);
    ?>
</body>

</html>
