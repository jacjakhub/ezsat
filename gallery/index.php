<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <title>Zespół Szkół AgroTechnicznych w Ropczycach Esport</title>
    <meta name="keywords" content="ezsat, zsat-ropczyce, esport, league of legends, Counter-Strike: Global Offensive, zsat" />
    <link href="../css/style.css" rel="stylesheet">
    <link href="css/gallery.css" rel="stylesheet">
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
    <link rel="Shortcut icon" href="../img/ico.ico" />
</head>

<body id="page-top">

    <style type="text/css">
      a{
          color: #ffffff;
      }
      a:hover, a:focus{
          color: #a9cc17;
          outline: none;
          text-decoration: none;
      }
      .gallery-item{
          padding: 5px;
      }
      .h-100{
          background-color: #141414;
      }
      #head_gallery{
          color: #fff;
          margin-top: 8%;
      }
      @media (max-width:480px){
          #head_gallery{
              margin-top: 22%;
          }
      }

    </style>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="../img/small_logo.png" class="img-responsive" style="height: 30px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <?php
            include("../navbar.php");
        ?>
    </nav>

    <!-- Page Content -->
    <div class="container">

      <!-- Page Heading -->
      <h1 id="head_gallery">E-ZSAT Gallery</h1>

      <div class="row">
        <?php
            $dirs = array_filter(glob('all_images/*'), 'is_dir');
            foreach($dirs as $val)
            {
                $path = str_replace("all_images/","",$val);
                $img = glob($val."/*");
                echo '<div class="col-lg-3 col-md-4 col-sm-6 gallery-item">';
                echo '<div class="card h-100">';
                echo '<div class="someId2"><a href="gallery.php?id='.$path.'"><img id="sized1" class="card-img-top" src="'.$img[0].'" alt=""></a></div>';
                echo '<div class="card-body">';
                echo '<h4 class="card-title">';
                echo '<a href="gallery.php?id='.$path.'">'.$path.'</a></h4></div></div></div>';
            }
        ?>
      </div>
      <!-- /.row -->
    </div>
    <!-- /.container -->

    <!-- Start footer -->
    <footer>
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="http://jakubbachorz.com/">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
      </div>
    </footer>

    <!-- Bootstrap and JavaScript -->
    <script src="../js/jquery/jquery.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>
    <script src="../js/script.js"></script>
</body>

</html>
