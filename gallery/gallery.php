<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta theme="description" content="">

    <title>Ezsat -  Gallery</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">
    <link rel="stylesheet" href="css/gallery.css">
    <link href="../css/style.css" rel="stylesheet">
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
  </head>

  <body>
    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="../img/small_logo.png" class="img-responsive" style="height: 30px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <?php
            include('../navbar.php');
        ?>
    </nav>

    <!-- Page Content -->
    <div class="container-fluid" style="padding-left: 5%;padding-right: 5%;">

        <div class="page-gallery">

            <div class="row">
                <?php
                    $image=glob("all_images/".$_GET['id']."/*.*");
                    if($image==null)
                        echo "all_images/".$_GET['id']."/*";
                    foreach($image as $val)
                    {
                        echo '<div class="someId1">';
                        echo '<a class="lightbox" href="'.$val.'">';
                        echo '<img id="sized1" src="'.$val.'" alt="'.$val.'"></a></div>';
                    }
                ?>
            </div>
        </div>
    </div>
    <!-- /.container -->

    <!-- Start footer -->
    <footer>
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
      </div>
    </footer>

    <!-- Bootstrap and JavaScript -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
    <script>
        baguetteBox.run('.page-gallery');
    </script>
</body>
</html>
