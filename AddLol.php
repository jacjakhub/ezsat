<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta theme="description" content="">

    <title>Ezsat -  Register</title>

    <link href="css/style.css" rel="stylesheet">
    <link href="css/bootstrap/css/bootstrap.css" rel="stylesheet">
  </head>

  <body>

    <!-- Page Content -->
    <div class="container-fluid" style="padding-left: 5%;padding-right: 5%;">
        <div id="info_exit">
        <?php
            include('admin/dbData.php');
            $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
            $name = $_POST['name'];
            $surname = $_POST['surname'];
            $email = $_POST['email'];
            $nick = $_POST['nickname'];
            $rank = $_POST['rank'];
            $class = $_POST['class'];
            $line = $_POST['line'];
            $team = $_POST['team'];
            $school = $_POST['school'];
            if(!$conn)
                echo "BLĄD POŁACZENIA <br> SKONTAKTUJ SIĘ Z ADMINISTRACJĄ";
            else if($name == null || $surname == null || $email == null || $nick == null || $rank == null || $class == null || $line == null || $team == null || $school == null)
                echo "NIEPOERAWNE DANE";
            else
            {
                $query = "INSERT INTO `participantslol`(`id`, `name`, `surname`, `email`, `nickname`, `rank`, `class`, `line`, `teamname`, `schollname`) VALUES (null,'".$name."','".$surname."','".$email."','".$nick."','".$rank."','".$class."','".$line."','".$team."','".$school."')";
                if(mysqli_query($conn,$query))
                {
                    echo "DZIĘKUJEMY ZA REJESTRACJĘ W KONKURSIE<br>";
                    echo "ZA CHWILĘ NASTĄPI PRZEKIEROWANIE";
                }
                else
                    echo "WYSTĄPIŁ BŁĄD PRZY DODAWANIU";
            }


        ?>
        <script>
            function Sleep(time)
            {
                return new Promise((resolve)=>setTimeout(resolve,time));
            }
            Sleep(3000).then(() =>
            {
                window.location.href = "..";
            });
        </script>
        </div>
    </div>
    <!-- /.container -->

    <!-- Start footer -->
    <footer style="margin-top: 40%">
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
      </div>
    </footer>
</body>
</html>
