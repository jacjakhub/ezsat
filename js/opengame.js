/*code open game button by Jakub Bachórz wszelkie prawa zastrzeżone*/
$(document).ready(function()
{
	var lolstate=0;
	var csstate=0;
    $('#buttonlol').click(function(){
        $('#leagueoflegends').toggle();
        $('#CounterStrikeGlobalOffensive').hide();
        $('#nonegame').hide();
		if(lolstate==0)
		{
			lolstate=1;
			csstate=0;
			$('#lol').attr('src', 'img/lol_active.png');
			$('#csgo').attr('src', 'img/cs_inactive.png');
		}
		else
		{
			lolstate=0;
			$('#lol').attr('src', 'img/lol_inactive.png');
			$('#nonegame').toggle();
		}

    });

    $('#buttoncsgo').click(function(){
        $('#CounterStrikeGlobalOffensive').toggle();
        $('#leagueoflegends').hide();
        $('#nonegame').hide();
		if(csstate==0)
		{
			csstate=1;
			lolstate=0;
			$('#csgo').attr('src', 'img/cs_active.png');
			$('#lol').attr('src', 'img/lol_inactive.png');
		}
		else
		{
			csstate=0;
			$('#csgo').attr('src', 'img/cs_inactive.png');
			$('#nonegame').toggle();
		}
    });
});
