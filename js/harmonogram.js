    /*
        Timetable
    */
$(document).ready(function() {
    $('.deliverable-infos').hide();
    $('.dropdown-deliverable').on('click', function(e) {
        console.log("dropdown toggled!");
        e.preventDefault();
        e.stopPropagation();
        var dataFor = $(this).data('for');
        var idFor = $(dataFor);
        idFor.slideToggle();
    });
});
