<?php
    session_start();
    if(!isset($_SESSION['logged']))
    {
        header('location: login.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>

    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="delete.js"></script>
</head>

<body>
    <!-- Start wrapper-->
    <div id="wrapper">
        <?php
            include 'menu.php';
        ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>DODAJ DO GALERII</h1>
                </div>
                <br>
                <form action="uploadGallery.php" method="post" enctype="multipart/form-data">
                    NAZWA FOLDERU <br> <input type="text" name="folderName"><br>
                    ZDJĘCIA <br> <input type="file" name="upFile[]" id="upFile" multiple><br>
                    <input type="submit" value="DODAJ">
                </form>
                <?php
                    if(isset($_SESSION['galleryAdd']))
                    {
                        echo "<br>".$_SESSION['galleryAdd']."<br>";
                        unset($_SESSION['galleryAdd']);
                    }
                ?>
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <h1>Wszystkie foldery</h1>
                    <?php
                    if(isset($_SESSION['delMsg']))
                    {
                        echo "<br>".$_SESSION['delMsg']."<br>";
                        unset($_SESSION['delMsg']);
                    }
                ?>
                </div>
                <ul>
                <?php
                    $dirs=array_filter(glob('../gallery/all_images/*'),"is_dir");
                    foreach($dirs as $val)
                    {
                        $abc='"'.$val.'"';
                        echo "<li><a href='editFolder.php?id=".str_replace("../gallery/all_images/","",$val)."'>".str_replace("../gallery/all_images/","",$val)."</a><button onclick='del($abc)' value='$val'>USUŃ</button></li>";
                    }
                ?>
                </ul>
            </div>
        </div>
    </div>
    <!-- End wrapper-->
</body>
</html>
