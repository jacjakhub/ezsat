<?php
     session_start();
     if(!isset($_SESSION['logged']))
            {
                unset($_SESSION['error']);
                header('location: login.php');
                exit();
            }
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>

    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="delete.js"></script>
    <script type="text/javascript" src="sort.js"></script>
    <?php
        include("dbData.php");
        $xmlDoc = simplexml_load_file("../config.xml") or die("ERROR");
        $config = $xmlDoc->children();
        global $socials;
        $socials=$config->socials;

        $conn =  mysqli_connect($db_host,$db_user,$db_pass,$db_name);
    ?>
</head>

<body>
    <!-- Start wrapper-->
    <div id="wrapper">
        <?php
            include 'menu.php';
        ?>
        <!-- Start navigation-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>EZSAT ADMINISTRACJA</h1>
                </div>
                <br>
                <div class="col-lg-12">
                    <form action="saveConfig.php" method="post" id="form1">
                        TEKST NA PASKU
                        <input type="text" id="line" name="line"><br>
                        LINK DO FACEBOOKA
                        <input type="text" id="facebook" name="facebook"><br>
                        LINK DO STRONY SZKOŁY
                        <input type="text" id="school" name="school"><br>
                        LINK DO YOUTUBE
                        <input type="text" id="youtube" name="youtube"><br>
                        LINK DO INSTAGRAMA
                        <input type="text" id="instagram" name="instagram"><br>
                        Zapisy do League of Legends <input type="checkbox" name="activeLol" id="activeLol"><br>
                        Zapisy do Counter Strike <input type="checkbox" name="activeCs" id="activeCs"><br>
                        <input type="submit" value="ZAPISZ ZMIANY">
                    </form>
                    <form action="uploadPdf.php" method="post" id="form2" enctype="multipart/form-data">
                        Regulamin lol <input type="file" name="rulesLol" id="rulesLol"><br>
                        zgoda rodziców lol <input type="file" name="permLol"><br>
                        Regulamin cs <input type="file" name="rulesCs"><br>
                        Zgoda rodziców cs <input type="file" name="permCs"><br>
                        <input type="submit" value="Zapisz pliki"><br>
                        <?php
                            if(isset($_SESSION['uploadMsg']))
                            {
                                echo $_SESSION['uploadMsg'];
                                unset($_SESSION['uploadMsg']);
                            }
                        ?>
                    </form>
                </div>
                <?php
                    if(isset($_SESSION['delMsg']))
                    {
                        echo $_SESSION['delMsg'];
                        unset($_SESSION['delMsg']);
                    }
                ?>
                <div class="col-md-12">
                    <h3>Zarejestrowani League of Legends</h3>
                    <button onclick="cleanTable('LeagueOfLegends')">Wyczyść tabelę</button>
                    <table class="table table-bordered" id="lol">
                        <tr>
                            <th onclick="sortTable(0,'lol')">Imię</th>
                            <th onclick="sortTable(1,'lol')">Nazwisko</th>
                            <th onclick="sortTable(2,'lol')">Adres Email</th>
                            <th onclick="sortTable(3,'lol')">Nick</th>
                            <th onclick="sortTable(4,'lol')">Linia</th>
                            <th onclick="sortTable(5,'lol')">Ranga</th>
                            <th onclick="sortTable(6,'lol')">Nazwa drużyny</th>
                            <th onclick="sortTable(7,'lol')">Klasa</th>
                            <th onclick="sortTable(8,'lol')">Szkoła</th>
                        </tr>
                        <?php
                            $query = "SELECT * FROM participantslol ORDER BY ID DESC";
                            $res = mysqli_query($conn,$query);
                            while($row=mysqli_fetch_array($res))
                            {
                                echo "<tr>";
                                echo "<td>".$row['name']."</td>";
                                echo "<td>".$row['surname']."</td>";
                                echo "<td>".$row['email']."</td>";
                                echo "<td>".$row['nickname']."</td>";
                                echo "<td>".$row['line']."</td>";
                                echo "<td>".$row['rank']."</td>";
                                echo "<td>".$row['teamname']."</td>";
                                echo "<td>".$row['class']."</td>";
                                echo "<td>".$row['schollname']."</td>";
                                echo "</tr>";
                            }
                            mysqli_free_result($res);
                        ?>
                    </table>
                </div>
                <div class="col-md-12">
                    <h3>Zarejestrowani Counter Strike</h3>
                    <button onclick="cleanTable('CounterStrike')">Wyczyść tabelę</button>
                    <table class="table table-bordered" id="cs">
                        <tr>
                            <th onclick="sortTable(0,'cs')">Imię</th>
                            <th onclick="sortTable(1,'cs')">Nazwisko</th>
                            <th onclick="sortTable(2,'cs')">Adres Email</th>
                            <th onclick="sortTable(3,'cs')">Nick</th>
                            <th onclick="sortTable(4,'cs')">Ranga</th>
                            <th onclick="sortTable(5,'cs')">Nazwa drużyny</th>
                            <th onclick="sortTable(6,'cs')">Klasa</th>
                            <th onclick="sortTable(7,'cs')">Szkoła</th>
                        </tr>
                        <?php
                            $query = "SELECT * FROM participantscs ORDER BY ID DESC";
                            $res = mysqli_query($conn,$query);
                            while($row=mysqli_fetch_array($res))
                            {
                                echo "<tr>";
                                echo "<td>".$row['name']."</td>";
                                echo "<td>".$row['surname']."</td>";
                                echo "<td>".$row['email']."</td>";
                                echo "<td>".$row['nickname']."</td>";
                                echo "<td>".$row['rank']."</td>";
                                echo "<td>".$row['teamname']."</td>";
                                echo "<td>".$row['class']."</td>";
                                echo "<td>".$row['schoolname']."</td>";
                                echo "</tr>";
                            }
                            mysqli_free_result($res);
                        ?>
                    </table>
                </div>
            </div>
        </div>
        <!-- End page-wrapper-->

        <script>
            if( <?php echo "'".$config->active->lol."'"; ?> == "true" )
                document.getElementById("activeLol").checked = true;
            else
                document.getElementById("activeLol").checked = false;
            if( <?php echo "'".$config->active->cs."'"; ?> == "true" )
                document.getElementById("activeCs").checked = true;
            else
                document.getElementById("activeCs").checked = false;

            document.getElementById("line").value = <?php echo "'".$config->linetext."'"; ?>;
            document.getElementById("facebook").value = <?php echo "'".$socials->facebook."'"; ?>;
            document.getElementById("school").value = <?php echo "'".$socials->school."'"; ?>;
            document.getElementById("youtube").value = <?php echo "'".$socials->youtube."'"; ?>;
            document.getElementById("instagram").value = <?php echo "'".$socials->instagram."'"; ?>;
        </script>


    </div>
    <!-- End wrapper-->
    <?php
        mysqli_close($conn);
    ?>
</body>
</html>
