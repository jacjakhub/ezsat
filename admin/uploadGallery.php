<?php
    session_start();
    function uploadFile($i)
    {
            $toUpload = true;
            $prefix = "../gallery/all_images/";
            $directory = $_POST['folderName'];

            if(!file_exists("../gallery/all_images"))
                mkdir("../gallery/all_images");

            if(!file_exists("../gallery/all_images/".$directory))
                mkdir("../gallery/all_images/".$directory);

            global $targetFile;
            $targetFile = $directory."/".basename($_FILES['upFile']['name'][$i]);
            $extension = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));

            if(isset($_POST['submit']))
            {
                $check = getimagesize($_FILES['upFile']['tmp_name'][$i]);
                if($check)
                    $toUpload=true;
                else
                {
                    echo "wymiary";
                    $toUpload=false;
                }

            }
            if(file_exists($targetFile))
            {
                echo "istnieje";
                $toUpload=false;
            }

            if($_FILES['upFile']['size'][$i]>50000000)
            {
                echo "rozmiar";
                $toUpload=false;
            }

            if($extension !== "jpg" && $extension !== "jpeg" && $extension !== "png" && $extension !== "gif")
            {
                echo "rozszerzenie";
                $toUpload=false;
            }

            if($toUpload)
            {
                if(move_uploaded_file($_FILES['upFile']['tmp_name'][$i],$prefix.$targetFile))
                    return true;
                else
                {
                    echo "nie dodadano";
                    return false;
                }

            }
            else
                return false;
    }
    $_SESSION['galleryAdd']="";
    $total = count($_FILES['upFile']['name']);
    for($i=0;$i<$total;$i++)
    {
        if(uploadFile($i))
            $_SESSION['galleryAdd'].="Dodano! ".$_FILES['upFile']['name'][$i]."<br>";
        else
            $_SESSION['galleryAdd'].="Nie dodano! ".$_FILES['upFile']['name'][$i]."<br>";
    }
    header("location: editGallery.php");
?>
