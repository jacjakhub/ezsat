<?php
    session_start();
    if(!isset($_SESSION['logged']))
    {
        header('location: login.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="script.js"></script>
    <script type="text/javascript" src="delete.js"></script>

    <?php
        include("dbData.php");
        $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
        $query = "SELECT * FROM article";
    ?>
</head>

<body>
    <!-- Start wrapper-->
    <div id="wrapper">
        <?php
            include 'menu.php';
        ?>

        <!-- Start page-wrapper-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Edytuj ogłoszenie</h1>
                    <?php
                        if(isset($_SESSION['delMsg']))
                        {
                            echo $_SESSION['delMsg']."<br>";
                            unset($_SESSION['delMsg']);
                        }
                    ?>
                </div>
            </div>
            <br><br>
            <ul>
                <?php
                    $result=mysqli_query($conn,$query);
                    while($value = mysqli_fetch_array($result))
                    {
                        $val=$value['id'];
                        echo "<li><a href='forms.php?status=".$value['id']."'>".$value['title']."</a><button onclick='delNews($val)'>USUŃ</button></li>";
                    }
                ?>
            </ul>
        </div>
        <!-- End page-wrapper-->
    </div>
    <!-- /.wrapper -->
    <?php
        mysqli_close($conn);
    ?>
</body>

</html>
