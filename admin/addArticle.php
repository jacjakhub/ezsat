<?php
        include("dbData.php");
        $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
        $status =$_GET['status'];
        $result='';

        function uploadFile()
        {
            $toUpload = true;
            $prefix = "../article/";
            $directory = "articleImages/";

            if(!file_exists("../article/".$directory))
                mkdir("../article/".$directory);

            global $targetFile;
            $targetFile = $directory.basename($_FILES['upFile']['name']);
            $extension = strtolower(pathinfo($targetFile,PATHINFO_EXTENSION));


            if(isset($_POST['submit']))
            {
                $check = getimagesize($_FILES['upFile']['tmp_name']);
                if($check)
                    $toUpload=true;
                else
                {
                    echo "wymiary";
                    $toUpload=false;
                }


            }

            if(file_exists($targetFile))
                                {
                    echo "istnieje";
                    $toUpload=false;
                }

            if($_FILES['upFile']['size']>50000000)
                               {
                    echo "rozmiar";
                    $toUpload=false;
                }

            if($extension !== "jpg" && $extension !== "jpeg" && $extension !== "png" && $extension !== "gif")
                               {
                    echo "rozszerzenie";
                    $toUpload=false;
                }

            if($toUpload)
            {
                if(move_uploaded_file($_FILES['upFile']['tmp_name'],$prefix.$targetFile))
                    return true;
                else
                {
                    echo "nie dodadano";
                    return false;
                }

            }
            else
                return false;
        }

        if($status=='added')
        {
            $title = $_POST['title'];
            $content = $_POST['content'];
            $author = $_POST['author'];
            $date = date("Y-m-d-H-i-s");

            if($title==''||$content==''||$author=='')
            {
                $result="NIEPOPRAWNE DANE";
            }
            else
            {
                if(uploadFile())
                {
                    $query ="INSERT INTO `article`(`id`, `title`, `content`, `image`, `author`, `date`) VALUES (null,'".$title."','".$content."','".$targetFile."','".$author."','".$date."')";
                    if(mysqli_query($conn,$query))
                        $result="DODANO POST";
                    else
                        $result="WYSTĄPIŁ BŁĄD PRZY DODAWANIU POSTU";
                }
                else
                    $result="WYSTĄPIŁ BŁĄD PRZY DODAWANIU PLIKU";
            }

        }
        else if($status=='edited')
        {
            $title = $_POST['title'];
            $content = $_POST['content'];
            $author = $_POST['author'];

            if($title==''||$content==''||$author=='')
            {
                $result="NIEPOPRAWNE DANE";
            }
            else
            {
                if(file_exists($_FILES['upFile']['tmp_name']) || is_uploaded_file($_FILES['upFile']['tmp_name']))
                {
                    uploadFile();
                    $delquery = "SELECT image FROM article WHERE id=".$_GET['id'];
                    $delres = mysqli_query($conn,$delquery);
                    $delvalue = mysqli_fetch_array($delres);

                    unlink("../article/".$delvalue['image']);

                    $query="UPDATE article SET title ='".$title."',content='".$content."',author='".$author."',image='".$targetFile."' WHERE id=".$_GET['id'];
                    if(mysqli_query($conn,$query))
                        $result="EDYTOWANO POST";
                    else
                        $result="BLAD EDYCJI Obrazek";
                }
                else
                {
                    $query="UPDATE article SET title ='".$title."',content='".$content."',author='".$author."' WHERE id=".$_GET['id'];
                    if(mysqli_query($conn,$query))
                        $result="EDYTOWANO POST";
                    else
                        $result="BLAD EDYCJI";
                }
            }

        }
        else if($status!='edited'&&$status!='added'&&$status!='new')
        {
            $query="SELECT * FROM article WHERE id=".$status;
            $queryres = mysqli_query($conn,$query);
            global $value;
            $value= mysqli_fetch_array($queryres);
        }
        mysqli_close($conn);
    ?>
