<?php
    session_start();
    if(!isset($_SESSION['logged']))
    {
        header('location: login.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>

    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="delete.js"></script>
</head>

<body>
    <!-- Start wrapper-->
    <div id="wrapper">

        <?php
            include 'menu.php';
        ?>
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>EDYTUJ ZDJĘCIA W GALERII</h1>
                    <?php
                        if(isset($_SESSION['delMsg']))
                        {
                            echo $_SESSION['delMsg']."<br>";
                            unset($_SESSION['delMsg']);
                        }
                    ?>
                </div>
                <form action="uploadGallery.php" method="post" enctype="multipart/form-data">
                    <?php $name =$_GET['id']; echo '<input type="text" value="'.$name.'" name="folderName" style="display:none;">'; ?>
                    ZDJĘCIA <br> <input type="file" name="upFile[]" id="upFile" multiple><br>
                    <input type="submit" value="DODAJ NOWE">
                </form>
                <?php
                    if(isset($_SESSION['galleryAdd']))
                    {
                        echo "<br>".$_SESSION['galleryAdd']."<br>";
                        unset($_SESSION['galleryAdd']);
                    }
                ?>
                <br>
                <?php
                    $imgs=glob('../gallery/all_images/'.$_GET['id']."/*");
                    foreach($imgs as $val)
                    {
                        $abc='"'.$val.'"';
                        echo "<div id='small'><img src='".$val."'><button onclick='del($abc)' value='$val'>USUŃ</button></div>";

                    }
                ?>

            </div>
        </div>
    </div>
    <!-- End wrapper-->
</body>
</html>
