function del(path)
{
    if(confirm("Czy chcesz usunąć "+path+"?"))
    {
        window.location.replace("deleteImage.php?image="+path);
    }
}

function delNews(path)
{
    if(confirm("Czy chcesz usunąć tego newsa?"))
    {
        window.location.replace("deleteNews.php?id="+path);
    }
}

function cleanTable(path)
{
    if(confirm("Czy chcesz wyczyścić tablę "+path+"?"))
    {
        window.location.replace("cleanDb.php?game="+path);
    }
}

function delTimetable(path)
{
    if(confirm("Czy usunać ten mecz?"))
    {
        window.location.replace("updateTimetable.php?status=delete&id="+path);
    }
}
