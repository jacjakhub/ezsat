<?php
    session_start();
    if(!isset($_SESSION['logged']))
    {
        header('location: login.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script type="text/javascript" src="script.js"></script>
    <script type="text/javascript" src="delete.js"></script>

    <?php
        global $value2;
        include("dbData.php");
        $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
        $query = "SELECT * FROM timetable";
        if($_GET['status']!='new')
        {
            $query2 = "SELECT * FROM timetable WHERE id=".$_GET['status'];
            $res2 = mysqli_query($conn,$query2);
            $value2 = mysqli_fetch_array($res2);
        }
    ?>
</head>

<body>
    <!-- Start wrapper-->
    <div id="wrapper">
        <?php
            include 'menu.php';
        ?>

        <!-- Start page-wrapper-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Edytuj harmonogram</h1>
                    <?php
                        if(isset($_SESSION['Msg']))
                        {
                            echo $_SESSION['Msg']."<br>";
                            unset($_SESSION['Msg']);
                        }
                    ?>
                </div>
            </div>
            <br><br>
            <form action="updateTimetable.php" method="post">
                <input type ="text" name="state" id="state" hidden>
                Druzyna 1 <input type="text" name="team1" id="team1"><br>
                Druzyna 2 <input type="text" name="team2" id="team2"><br>
                Data <input type="date" name="date" id="date"><br>
                Godzina <input type="time" name="time" id="time"><br>
                Wynik <input type="text" name="score" id="score"><br>
                Status <select name="status" id="status">
                    <option value="waiting">Oczekujący</option>
                    <option value="ongoing">Trwający</option>
                    <option value="finished">Zakończony</option>
                    <option value="cancelled">Odwołany</option>
                </select><br>
                <input type="submit" value="Dodaj mecz">
            </form>
            <h3>Aktualne mecze</h3>
            <ul>
                <?php
                    $result=mysqli_query($conn,$query);
                    while($value = mysqli_fetch_array($result))
                    {
                        $val=$value['id'];
                        echo "<li>".$value['team1']." ".$value['score']." ".$value['team2']." ".$value['date']." ".$value['status'];
                        echo " <a href='editTimetable.php?status=".$value['id']."'>Edytuj</a> <button onclick='delTimetable($val)'>USUŃ</button></li>";
                    }
                ?>
            </ul>
        </div>
        <!-- End page-wrapper-->
    </div>
    <!-- /.wrapper -->
    <script>
        document.getElementById("state").value = "<?php echo $_GET['status']; ?>";
        if(document.getElementById("state").value != 'new')
        {
            var d="<?php echo $value2['date']; ?>";
            var date = Date.parse(d);
            var d1=new Date(date);

            var hour;
            var minutes;

            var currentDate = d1.toISOString().slice(0,10);

            if(d1.getHours()<10)
                hour="0"+d1.getHours();
            else
                hour=d1.getHours();

            if(d1.getMinutes()<10)
                minutes="0"+d1.getMinutes();
            else
                minutes=d1.getMinutes();

            var currentTime = hour + ':' + minutes;

            document.getElementById("team1").value = "<?php echo $value2['team1']; ?>";
            document.getElementById("team2").value = "<?php echo $value2['team2']; ?>";
            document.getElementById("date").value = currentDate;
            document.getElementById("time").value = currentTime;
            document.getElementById("score").value = "<?php echo $value2['score']; ?>";
            document.getElementById("status").value = "<?php echo $value2['status']; ?>";
        }
    </script>
    <?php
        mysqli_close($conn);
    ?>
</body>

</html>
