<!-- Start navigation-->
    <div class="navbar-default sidebar" role="navigation">
        <?php
            echo "ZALOGOWANO JAKO: ".$_SESSION['user'];
        ?>
        <div class="sidebar-nav navbar-collapse">
            <ul class="nav flex-column" id="side-menu">
                <li class="nav-item">
                    <a class="nav-link active" href="index.php"><i class="fa fa-home fa-fw"></i> Strona główna</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="forms.php?status=new"><i class="fa fa-edit fa-fw"></i> Dodaj ogłoszenie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="articleslist.php"><i class="fa fa-edit fa-fw"></i> Edytuj ogłoszenie</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="editTimetable.php?status=new"><i class="fa fa-edit fa-fw"></i> Edytuj harmonogram</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="editGallery.php"><i class="fa fa-edit fa-fw"></i> Edytuj galerię</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="log_out.php"><i class="fa fa-times fa-fw"></i> Wyloguj</a>
                </li>
            </ul>
        </div>
    </div>
    <!-- End navigation-->
