<?php
    session_start();
    if(isset($_SESSION['logged']) && $_SESSION['logged']==true)
    {
        if(isset($_GET['game']))
        {
            include("dbData.php");
            $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
            if($_GET['game']=="LeagueOfLegends")
            {
                $query="DELETE FROM participantslol";
                if(mysqli_query($conn,$query))
                {
                    $_SESSION['delMsg']="Pomyślnie wyczyszczono tabelę";
                    header("location: index.php");
                    exit();
                }
                else
                {
                    $_SESSION['delMsg']="Wystapił błąd";
                    header("location: index.php");
                    exit();
                }
            }
            if($_GET['game']=="CounterStrike")
            {
                $query="DELETE FROM participantscs";
                if(mysqli_query($conn,$query))
                {
                    $_SESSION['delMsg']="Pomyślnie wyczyszczono tabelę";
                    header("location: index.php");
                    exit();
                }
                else
                {
                    $_SESSION['delMsg']="Wystapił błąd";
                    header("location: index.php");
                    exit();
                }
            }
        }
        header("location: index.php");
    }
    header("location: index.php");
?>
