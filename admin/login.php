<?php
    session_start();
    if(isset($_SESSION['logged']))
    {
        header('location: index.php');
        exit();
    }

?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>

    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

</head>

<body>
    <!-- Start container-->
    <div class="container">
        <!-- Start row-->
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div>
                    <h3>Panel Administracyjny</h3>
                </div>
                <!-- Start form-->
                <form action="log_in.php" method="post">
                    <fieldset>
                        <div class="form-group">
                            <input class="form-control" placeholder="Login" name="login" type="login" autofocus>
                        </div>
                        <div class="form-group">
                            <input class="form-control" placeholder="Password" name="password" type="password">
                        </div>
                            <input type="submit" class="btn btn-lg btn-success btn-block" value="Zaloguj się">
                    </fieldset>
                </form>
                <?php
                    if(isset($_SESSION['error']))
                    {
                        echo $_SESSION['error'];
                        unset($_SESSION['error']);
                    }
                ?>
                <!-- End form-->
            </div>
        </div>
        <!-- End row-->
    </div>
    <!-- End container-->
</body>

</html>
