<?php
    session_start();
    if(!isset($_SESSION['logged']))
    {
        header('location: login.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pl">

<head>

    <meta charset="utf-8">
    <title>E-ZSAT Admin</title>
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css">
    <link href="css/style-admin.css" rel="stylesheet" type="text/css">
    <link href="../css/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <script src="script.js"></script>

    <?php
        include("addArticle.php");
    ?>
</head>

<body>
    <!-- Start wrapper-->

    <div id="wrapper">
        <?php
            include 'menu.php';
        ?>

        <!-- Start page-wrapper-->
        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1>Dodaj ogłoszenie</h1>
                </div>
            </div>

            <?php
                echo "<br>".$result."<br>";
            ?>
                <!-- Start form-->
                <?php
                    if($status == 'new' || $status == 'added' || $status == 'edited')
                        echo '<form action="forms.php?status=added" method="post" enctype="multipart/form-data">';
                    else
                        echo '<form action="forms.php?status=edited&id='.$status.'" method="post" enctype="multipart/form-data">';
                ?>
                    <div class="form-group">
                        <label>Temat</label>
                        <input id="title" type="text" class="form-control" placeholder="Wpisz temat" name="title">
                    </div>
                    <div class="form-group">
                        <label>Zdjęcie główne</label><br>
                        <input type="file" name="upFile" id="upFile">
                    </div>
                    <div class="form-group">
                        <label>Autor</label>
                        <input id="author" type="text" class="form-control" placeholder="Wpisz autora" name="author">
                    </div>

                    <div class="form-group">
                        <label>Treść</label><br>
                        <button type="button" class="btn btn-sm btn-default" onclick="makeBold()" ><i class="fa fa-bold"></i></button>
                        <button type="button" class="btn btn-sm btn-default" onclick="makeItalic()" ><i class="fa fa-italic"></i></button>
                        <button type="button" class="btn btn-sm btn-default" onclick="makeStrike()"><i class="fa fa-strikethrough"></i></button>
                        <button type="button" class="btn btn-sm btn-default" onclick="makeLink()"><i class="fa fa-link"></i></button>
                        <button type="button" class="btn btn-sm btn-default" onclick=""><i class="fa fa-chain-broken"></i></button>
                        <button type="button" class="btn btn-sm btn-default" onclick=""><i class="fa fa-list"></i></button>

                        <textarea onmouseup="getSelectedText()" id="content" class="form-control" rows="10" name="content"></textarea>
                    </div>
                        <button type="submit" class="btn btn-default" name="submit"><i class="fa fa-check"></i> Wyślij post</button>
                        <button type="reset" class="btn btn-default"><i class="fa fa-times"></i> Usuń post</button>
                 </form>
                 <!-- End form-->
        </div>
        <!-- End page-wrapper-->
    </div>
    <!-- /.wrapper -->

        <script>
        var title = document.getElementById('title');
        var author = document.getElementById('author');
        var content = document.getElementById('content');

        title.value= <?php echo json_encode($value['title']); ?>;
        author.value=<?php echo json_encode($value['author']); ?>;
        content.value=<?php echo json_encode($value["content"]); ?>;
    </script>
</body>

</html>
