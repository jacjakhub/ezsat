var finish;
var start;
var content;

function makeBold()
{
    content = document.getElementById('content');
    var newContent = content.value.slice(0,start-1)+content.value.slice(start,finish).bold()+content.value.slice(finish+1);
    content.value = newContent;
}

function makeItalic()
{
    content = document.getElementById('content');
    var newContent = content.value.slice(0,start-1)+content.value.slice(start,finish).italics()+content.value.slice(finish+1);
    content.value = newContent;
}

function makeStrike()
{
    content = document.getElementById('content');
    var newContent = content.value.slice(0,start-1)+content.value.slice(start,finish).strike()+content.value.slice(finish+1);
    content.value = newContent;
}

function makeLink()
{
    content = document.getElementById('content');
    var link = prompt('Podaj adres odnośnika');
    var newContent = content.value.slice(0,start-1)+content.value.slice(start,finish).link(link)+content.value.slice(finish+1);
    content.value = newContent;
}

function getSelectedText () {
    content = document.getElementById('content');
    start = content.selectionStart;
    finish = content.selectionEnd;
}
