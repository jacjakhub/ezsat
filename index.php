<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
  <title>Zespół Szkół AgroTechnicznych w Ropczycach Esport</title>
  <meta name="keywords" content="ezsat, zsat-ropczyce, esport, league of legends, Counter-Strike: Global Offensive, zsat" />
  <link rel="stylesheet" href="css/style.css">
  <link href="css/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
  <link rel="Shortcut icon" href="img/ico.ico" />
  <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">

  <?php
    include('admin/dbData.php');
    $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
    $xmlDoc = simplexml_load_file("config.xml") or die("ERROR");
    $config = $xmlDoc->children();
    global $socials;
    global $filesCs;
    global $filesLol;
    global $active;
    global $sliderPhotos;

    $sliderPhotos=$config->slider;
    $socials=$config->socials;
    $filesCs=$config->files->cs;
    $filesLol=$config->files->lol;
    $active=$config->active;
  ?>
</head>

<body id="page-top">

    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="img/small_logo.png" class="img-responsive" style="height: 30px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#page-top">Strona główna</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#news">Aktualności</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#page2">Informacje</a>
            </li>
            <li class="nav-item">
              <a class="nav-link js-scroll-trigger" href="#contact">Kontakt</a>
            </li>
          </ul>
        </div>
    </nav>

    <!-- Start Slider -->
    <header class="slider">
      <div id="textdiv"><div id=textslider>ZSAT E-SPORT LEAGUE</div></div>
      <div class="slider slider--full">
          <div id="vinieta"></div>
          <img class="greydout1" src="img/square/1.png" style="width: 100%; height: 100%; position: absolute; z-index: 2; background: no-repeat 50% 50%;"/>
          <img class="greydout2" src="img/square/2.png" style="width: 100%; height: 100%; position: absolute; z-index: 2; background: no-repeat 50% 50%;"/>
          <img class="greydout3" src="img/square/3.png" style="width: 100%; height: 100%; position: absolute; z-index: 2; background: no-repeat 50% 50%;"/>
          <img class="greydout4" src="img/square/4.png" style="width: 100%; height: 100%; position: absolute; z-index: 2; background: no-repeat 50% 50%;"/>
          <img class="greydout5" src="img/square/5.png" style="width: 100%; height: 100%; position: absolute; z-index: 2; background: no-repeat 50% 50%;"/>

        <div class="slider__inner">
          <input checked type="radio" name="slider__radiobox" id="slider__radiobox--1" class="slider__radiobox slider__radiobox--1">
          <label for="slider__radiobox--1" class="slider__radiobox-label slider__radiobox-label--item slider__radiobox-label--item--1"></label>
          <input type="radio" name="slider__radiobox" id="slider__radiobox--2" class="slider__radiobox slider__radiobox--2">
          <label for="slider__radiobox--2" class="slider__radiobox-label slider__radiobox-label--item slider__radiobox-label--item--2"></label>
          <input type="radio" name="slider__radiobox" id="slider__radiobox--3" class="slider__radiobox slider__radiobox--3">
          <label for="slider__radiobox--3" class="slider__radiobox-label slider__radiobox-label--item slider__radiobox-label--item--3"></label>
          <input type="radio" name="slider__radiobox" id="slider__radiobox--4" class="slider__radiobox slider__radiobox--4">
          <label for="slider__radiobox--4" class="slider__radiobox-label slider__radiobox-label--item slider__radiobox-label--item--4"></label>

          <div class="slider__slides">
            <svg class="slider__slide">
              <defs>
                <filter id="colors">
                  <feColorMatrix result="A" in="SourceGraphic" type="matrix"
                   values="0.3333 0.3333 0.3333 0 0
                           0.3333 0.3333 0.3333 0 0
                           0.3333 0.3333 0.3333 0 0
                           0      0      0      1 0"/>
                  <feColorMatrix color-interpolation-filters="sRGB" in="A" type="matrix"
                   values="0.7000 0      0.1000 0 0
                           0.8500 0      0      0 0
                           0      0      0      0 0
                           0      0      0      1 0 "/>
                </filter>
              </defs>

              <image class="slider__slide--1" filter="url('#colors')" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" href="img/backgrounds/1.jpg"></image>
            </svg>

            <svg class="slider__slide">
              <image class="slider__slide--2" filter="url('#colors')" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" href="img/backgrounds/2.jpg"></image>
            </svg>

            <svg class="slider__slide">
              <image class="slider__slide--3" filter="url('#colors')" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" href="img/backgrounds/3.jpg"></image>
            </svg>

            <svg class="slider__slide">
              <image class="slider__slide--4" filter="url('#colors')" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" href="img/backgrounds/4.jpg"></image>
            </svg>
          </div>
        </div>
      </div>

    </header>

    <div class="news container-fluid" id="news">
        <div class="row">
            <div id="line-live"><p><b>Informacje na Żywo:</b> <?php $text=$config->linetext; echo $text; ?> </p></div>
            <div id="social" class="col-md-4">
                <h4>SOCIAL MEDIA</h4>
                <ul class="social-icons">
                  <i class="fa fa-facebook"></i><li><a id="facebook" href="">@ZSATeSportsLeague</a></li>
                  <i class="fa fa-graduation-cap"></i><li><a id="school" href="">www.zsat-ropczyce.pl</a></li>
                  <i class="fa fa-youtube-play"></i><li><a id="youtube" href="">@Zespół Szkół Agro-Technicznych Ropczyce</a></li>
                  <i class="fa fa-instagram"></i><li><a id="instagram" href="">@zsat_ropczyce</a></li>
                </ul>
                <h4>HARMONOGRAM</h4>
                  <table cellpadding="10" class="table">
                    <tbody>
                        <?php
                            $query = "SELECT * FROM timetable ORDER BY date DESC";
                            $result = mysqli_query($conn,$query);
                            for($i=0;$i<3;$i++)
                            {
                                if($val = mysqli_fetch_array($result))
                                {
                                    if($val['status']=='cancelled')
                                        $icon="timetable/icon/red.png";
                                    else if($val['status']=='waiting'||$val['status']=='ongoing')
                                        $icon="timetable/icon/yellow.png";
                                    else
                                        $icon="timetable/icon/green.png";

                                    echo "<tr>";
                                    echo '<td class="title">'.$val['team1'].' vs '.$val['team2'].'</td>';
                                    echo '<td class="score">'.$val['score'].'</td>';
                                    echo '<td class="score">'.$val['date'].'</td>';
                                    echo '<td class="status"><img src="'.$icon.'"></td>';
                                    echo "</tr>";
                                }
                            }
                            mysqli_free_result($result);
                        ?>
                    </tbody>
                  </table>
                <a href="timetable/" style="text-align: center;">ZOBACZ HISTORIĘ GIER</a>
            </div>

            <div id="freshnews" class="col-md-8">
              <h1><strong>Aktualności</strong></h1>
              <h5><b>Najnowsze publikacje</b></h5>
                <div id="posts" class="col-md">
                    <?php
                        $query = "SELECT * FROM article ORDER BY ID DESC LIMIT 3";
                        $result = mysqli_query($conn,$query);
                        $row=mysqli_fetch_array($result);
                        echo "<a href='article/article.php?id=".$row['id']."'><div id='big_post'>";
                            echo "<img id='big' src='article/".$row['image']."'>";
                            echo "<div id='post_text'>".$row['title']."</div>";
                        echo "</div></a>";

                        $row=mysqli_fetch_array($result);
                        echo "<a href='article/article.php?id=".$row['id']."'><div id='small_post'>";
                            echo "<img src='article/".$row['image']."'>";
                            echo "<div id='post_text'>".$row['title']."</div>";
                        echo "</div></a>";

                        $row=mysqli_fetch_array($result);
                        echo "<a href='article/article.php?id=".$row['id']."'><div id='small_post'>";
                            echo "<img src='article/".$row['image']."'>";
                            echo "<div id='post_text'>".$row['title']."</div>";
                        echo "</div></a>";
                        mysqli_free_result($result);
                    ?>
                </div>
                <h4><a href="article/?page=1"><strong>WIĘCEJ INFORMACJI</strong></a></h4>
              <div class="clear"></div>
            </div>
        </div>
    </div>
    <div id="page2">
	   <div id="buttons">
        <a id="buttonlol" ><img id="lol" src="img/lol_inactive.png"></a>
        <a id="buttoncsgo" ><img id="csgo" src="img/cs_inactive.png"></a>
	   </div>
        <div class="clear"></div>

        <div id="nonegame">
            <h1>Wybierz gre aby wyświetlić zawartość</h1>
        </div>

        <div id="leagueoflegends" style="display:none;">
          <div id="text">
            <h1>GALERIA <span>ZDJĘĆ</span></h1>
            <a href="gallery/index.php">WYŚWIETL CAŁĄ CALERIĘ</a>
          </div>
          <div id="gallery">
            <?php
                $images=glob("gallery/all_images/*/*");
                for($i=0;$i<6;$i++)
                {
                    $number=rand(0,count($images)-1);
                    echo "<div class='someId'><img id='sized' src='$images[$number]'></div>";
                }
            ?>
          </div>

          <div class="game_info">
            <div class="col-md-4 game-left">
              <h2>Wymagania oraz Regulamin</h2>
                <p>E-ZSAT to ekipa e-sportowa organizująca turnieje, podczas których rywalizują ze sobą gracze w wielu gier.
                  Team E-ZSAT obejmuje organizację szkolnych i międzyszkolnych rozgrywek ligowych i turniejów,
                  takich jak Masters Counter-Strike: Global Offensive, League of Legends. Oto instrukcja pomagająca w rejestracji do turnieju:<br><br>

                  Wypełnianie formularza rejestracyjnego. Wymagamy podanie poprawnego adresu e-mail, imienia, nazwiska, pseudonimu, dywizji, klasa, linii. Należy również potwierdzić że zapoznałem/am się z Regulaminem E-ZSAT, a także przeczytać i zaakceptować warunki ochrony i wykorzystania danych. Gdy wszystko jest gotowe wystarczy kliknąć przycisk „Zapisz się”.<br><br>

                  Potwierdzenie rejestracji. Po wysłaniu danych, otrzymasz e-mail od administratora, czy możesz brać udział w turnieju. Uwaga: Możliwe, że twoja skrzynka pocztowa potraktuje naszą automatyczną wiadomość jako spam! Sprawdź folder „spam” w przypadku braku naszego maila.<br><br>

                  Uwaga! Każdy uczestnik musi się zapisać. Jeśli twoja drużyna nie jest na liście albo się pomyliłeś, skontaktuj się na adres E-mail rekrutacja@ezsat.pl <br></p>

                <a id="rulesLol" href=""><input type="submit" value="Regulamin Turnieju EZSAT.pdf"><br></a>
                <a id="permsLol" href=""><input type="submit" value="Zgoda Rodzica Turnieju EZSAT.pdf"></a>
              </div>
              <div class="col-md-8 game-right">
                <div class="game">
                  <h2>Formularz Zgłoszeniowy</h2>
                  <form action="AddLol.php" method="post">
                    <div id="inactiveRegLol">
                        OBECNIE NIE PROWADZIMY ZAPISÓW NA TEN TURNIEJ
                    </div>
                    <div id="activeRegLol" class="form">
                      <div>
                        <span>Imię<label>*</label></span>
                        <input type="text" name="name" placeholder="Wpisz imię *" required="required" />
                      </div>
                      <div>
                        <span>Nazwisko<label>*</label></span>
                        <input type="text" name="surname" placeholder="Wpisz nazwisko *" required="required" />
                      </div>
                      <div>
                        <span>Adres E-mail<label>*</label></span>
                        <input type="text" name="email" placeholder="Wpisz e-mail *" required="required" />
                      </div>
                      <div>
                        <span>Pseudonim<label>*</label></span>
                        <input type="text" name="nickname" placeholder="Wpisz pseudonim *" required="required" />
                      </div>
                      <div>
                        <span>Dywizja <label style="color:red;">(Sezon poprzedni)*</label></span>
                        <input type="text" name="rank" placeholder="Wpisz dywizje Gold IV *" required="required" />
                      </div>
                      <div>
                        <span>Klasa<label>*</label></span>
                        <input type="text" name="class" placeholder="Wpisz klasa I Tig *" required="required" />
                      </div>
                      <div>
                        <span>Linia<label>*</label></span>
                        <input type="text" name="line" placeholder="Wpisz linie Mid *" required="required" />
                      </div><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                      <div>
                        <span>Nazwa Drużyny<label>*</label></span>
                        <input type="text" name="team" placeholder="Wpisz nazwa drużyny *" required="required"/>
                      </div>
                      <div>
                        <span>Nazwa Szkoły<label>*</label></span>
                        <input type="text" name="school" placeholder="Wpisz nazwe pełną swojej szkoły *" required="required" />
                      </div>
                      <div class="clear"> </div>
                      <br>
                      <label><input type="checkbox" required="required">Oświadczam, że zapoznałem/am się z Regulaminem E-ZSAT</label><br><br>
                      <input type="submit" value="Zapisz się" name="button_send">
                      </div>
                  </form>
                </div>
              </div>
            </div>
        </div>

        <div id="CounterStrikeGlobalOffensive" style="display:none;">
            <div id="text">
                <h1>GALERIA <span>ZDJĘĆ</span></h1>
                <a href="gallery/index.php">WYŚWIETL CALĄ CALERIĘ</a>
            </div>
            <div id="gallery">
                <?php
                    $images=glob("gallery/all_images/*/*");
                    for($i=0;$i<6;$i++)
                    {
                        $number=rand(0,count($images)-1);
                        echo "<div class='someId'><img id='sized' src='$images[$number]'></div>";
                    }
                ?>
            </div>
            <div class="game_info">
              <div class="col-md-4 game-left">
                <h2>Wymagania oraz Regulamin</h2>
                  <p>E-ZSAT to ekipa e-sportowa organizująca turnieje, podczas których rywalizują ze sobą gracze w wielu gier.
                    Team E-ZSAT obejmuje organizację szkolnych i międzyszkolnych rozgrywek ligowych i turniejów,
                    takich jak Masters Counter-Strike: Global Offensive, League of Legends. Oto instrukcja pomagająca w rejestracji do turnieju:<br><br>

                    Wypełnianie formularza rejestracyjnego. Wymagamy podanie poprawnego adresu e-mail, imienia, nazwiska, pseudonimu, rangi, klasy. Należy również potwierdzić że zapoznałem/am się z Regulaminem E-ZSAT, a także przeczytać i zaakceptować warunki ochrony i wykorzystania danych. Gdy wszystko jest gotowe wystarczy kliknąć przycisk „Zapisz się”.<br><br>

                    Potwierdzenie rejestracji. Po wysłaniu danych, otrzymasz e-mail od administratora, czy możesz brać udział w turnieju. Uwaga: Możliwe, że twoja skrzynka pocztowa potraktuje naszą automatyczną wiadomość jako spam! Sprawdź folder „spam” w przypadku braku naszego maila.<br><br>

                    Uwaga! Każdy uczestnik musi się zapisać. Jeśli twoja drużyna nie jest na liście albo się pomyliłeś, skontaktuj się na adres E-mail rekrutacja@ezsat.pl <br></p>

                <a id="rulesCs" href=""><input type="submit" value="Regulamin Turnieju EZSAT.pdf"><br></a>
                <a id="permsCs" href=""><input type="submit" value="Zgoda Rodzica Turnieju EZSAT.pdf"></a>
              </div>
              <div class="col-md-8 game-right">
                <div class="game">
                  <h2>Formularz Zgłoszeniowy</h2>
                  <form action="AddCs.php" method="post">
                    <div id="inactiveRegCs">
                      OBECNIE NIE PROWADZIMY ZAPISÓW NA TEN TURNIEJ
                    </div>
                    <div id="activeRegCs" class="form">
                      <div>
                        <span>Imię<label>*</label></span>
                        <input type="text" name="name" placeholder="Wpisz imię *" required="required" />
                      </div>
                      <div>
                        <span>Nazwisko<label>*</label></span>
                        <input type="text" name="surname" placeholder="Wpisz nazwisko *" required="required" />
                      </div>
                      <div>
                        <span>Adres E-mail<label>*</label></span>
                        <input type="text" name="email" placeholder="Wpisz e-mail *" required="required" />
                      </div>
                      <div>
                        <span>Pseudonim<label>*</label></span>
                        <input type="text" name="nickname" placeholder="Wpisz pseudonim *" required="required" />
                      </div>
                      <div>
                        <span>Ranga <label>*</label></span>
                        <input type="text" name="rank" placeholder="Wpisz range Gold IV *" required="required" />
                      </div>
                      <div>
                        <span>Klasa<label>*</label></span>
                        <input type="text" name="class" placeholder="Wpisz klasa I Tig *" required="required" />
                      </div>
                      <div>
                        <span>Nazwa Drużyny<label>*</label></span>
                        <input type="text" name="team" placeholder="Wpisz nazwa drużyny *" required="required"/>
                      </div>
                      <div>
                        <span>Nazwa Szkoły<label>*</label></span>
                        <input type="text" name="school" placeholder="Wpisz nazwe pełną swojej szkoły *" required="required" />
                      </div>
                      <div class="clear"> </div>
                      <br>
                      <label><input type="checkbox" required="required">Oświadczam, że zapoznałem/am się z Regulaminem E-ZSAT</label><br><br>
                      <input type="submit" value="Zapisz się" name="button_send">
                    </div>
                  </form>
                </div>
              </div>
            </div>
        </div>
    </div>
    <!--Start Contact -->
    <section class="contact" id="contact">
      <div id="map"></div>
      <div class="contact">
        <div class="container">
          <form action="mail.php" method="post">
            <div class="form">
              <h3>KONTAKT</h3>

              <div>
                <span>Imię<label>*</label></span>
                <input type="text" name="form_name" placeholder="Wpisz Imię *" required="required" />
              </div>

              <div>
                <span>Adres E-mail<label>*</label></span>
                <input type="text" name="form_email" placeholder="Wpisz E-mail *" required="required" />
              </div>

              <center><span>Wiadomość<label>*</label></span></center>
              <textarea name="form_content" rows="5" required="required" placeholder="Wpisz Wiadomość *"></textarea>

              <div class="clear"> </div><br><br>
            </div><br>
            <div class="clear"> </div>
            <input type="submit" value="Wyślij wiadomość" name="button_send">
          </form>
        </div>
      </div>
    </section>
    <!-- Start footer -->
    <footer>
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="#">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
      </div>
    </footer>

    <!-- Bootstrap and JavaScript -->
    <script>
        document.getElementById("facebook").href = <?php echo "'".$socials->facebook."'"; ?>;
        document.getElementById("school").href = <?php echo "'".$socials->school."'"; ?>;
        document.getElementById("youtube").href = <?php echo "'".$socials->youtube."'"; ?>;
        document.getElementById("instagram").href = <?php echo "'".$socials->instagram."'"; ?>;

        document.getElementById("rulesCs").href = <?php echo "'".$filesCs->rules."'"; ?>;
        document.getElementById("permsCs").href = <?php echo "'".$filesCs->perm."'"; ?>;
        document.getElementById("rulesLol").href = <?php echo "'".$filesLol->rules."'"; ?>;
        document.getElementById("permsLol").href = <?php echo "'".$filesLol->perm."'"; ?>;

        var activeLol = <?php echo "'".$active->lol."'"; ?>;
        var activeCs = <?php echo "'".$active->cs."'"; ?>;

        if(activeLol=="true")
        {
            document.getElementById("activeRegLol").style.display = "block";
            document.getElementById("inactiveRegLol").style.display = "none";
        }
        else
        {
            document.getElementById("activeRegLol").style.display = "none";
            document.getElementById("inactiveRegLol").style.display = "block";
        }
        if(activeCs=="true")
        {
            document.getElementById("activeRegCs").style.display = "block";
            document.getElementById("inactiveRegCs").style.display = "none";
        }
        else
        {
            document.getElementById("activeRegCs").style.display = "none";
            document.getElementById("inactiveRegCs").style.display = "block";
        }
    </script>
    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="js/bootstrap/js/bootstrap.min.js"></script>
    <script src="js/jquery-easing/jquery.easing.min.js"></script>
    <script src="js/script.js"></script>
    <script src="js/opengame.js"></script>
    <script src="js/harmonogram.js"></script>
    <script src="js/slider.js"></script>
    <!-- Javascript googlemaps -->
    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyABcnRe7E5bOel7BdU4xA_mQqVps0F6uus&callback=initMap"></script>
    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', init);

        function init() {
            // Basic options for a simple Google Map
            // For more options see: https://developers.google.com/maps/documentation/javascript/reference#MapOptions
            var mapOptions = {
                // How zoomed in you want the map to start at (always required)
                zoom: 18,

                // The latitude and longitude to center the map (always required)
                center: new google.maps.LatLng(50.053815, 21.61478,15), // ZSAT Ropczyce

                // How you would like to style the map.
                // This is where you would paste any style found on Snazzy Maps.
                styles: [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#ffffff"},{"lightness":40}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#000000"},{"lightness":16}]},{"featureType":"all","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"},{"lightness":20},{"visibility":"on"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#00574d"},{"lightness":17},{"weight":1.2},{"visibility":"on"}]},{"featureType":"administrative","elementType":"labels.text","stylers":[{"color":"#93c991"},{"visibility":"simplified"}]},{"featureType":"landscape","elementType":"all","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":20},{"visibility":"on"}]},{"featureType":"landscape.natural","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#1c1c1c"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#272727"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#272727"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#000000"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#000000"},{"lightness":21}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#a9cc17"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#000000"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#a9cc17"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#3c480b"},{"lightness":16}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#9d1111"},{"lightness":19}]},{"featureType":"water","elementType":"geometry","stylers":[{"color":"#1b1934"},{"lightness":17}]}]
            };

            // Get the HTML DOM element that will contain your map
            // We are using a div with id="map" seen below in the <body>
            var mapElement = document.getElementById('map');

            // Create the Google Map using our element and options defined above
            var map = new google.maps.Map(mapElement, mapOptions);

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(50.053729, 21.614608),
                map: map,
                title: 'Snazzy!'
            });
        }
    </script>
</body>
</html>
