<!DOCTYPE html>
<html lang="pl">

<head>
    <meta charset="utf-8">
    <title>Zespół Szkół AgroTechnicznych w Ropczycach Esport</title>
    <meta name="keywords" content="ezsat, zsat-ropczyce, esport, league of legends, Counter-Strike: Global Offensive, zsat" />
    <link href="../css/style.css" rel="stylesheet">
    <link href="css/timetable.css" rel="stylesheet">
    <link href="../css/bootstrap/css/bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100" rel="stylesheet">
    <link rel="Shortcut icon" href="../img/ico.ico" />
    <?php
        include("../admin/dbData.php");
        $conn = mysqli_connect($db_host,$db_user,$db_pass,$db_name);
        $query = "SELECT * FROM timetable ORDER BY date DESC";
        $result = mysqli_query($conn,$query);
    ?>
</head>

<body id="page-top">

    <style type="text/css">
      body{
        color: #fff;
      }
      table{
          border-collapse: separate;
      }
    </style>

    <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top"><img src="../img/small_logo.png" class="img-responsive" style="height: 30px;"></a>
        <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          Menu
          <i class="fa fa-bars"></i>
        </button>
        <?php
            include("../navbar.php");
        ?>
    </nav>

    <div class="container-fluid">
      <h1>Harmonogram</h1>
      <h2>COUNTER-STRIKE | LEAGUE OF LEGENDS | E-ZSAT</h2>


      <div class="row">
        <div class="col-sm" style="color: red">
          <img src="icon/red.png">
          Odwołany
        </div>
        <div class="col-sm" style="color: yellow">
          <img src="icon/yellow.png">
          Oczekujący
        </div>
        <div class="col-sm" style="color: #a9cc17">
          <img src="icon/green.png">
          Zakończony
        </div>
      </div><br>

            <table cellpadding="10">
              <tbody>
                <?php
                    while($val = mysqli_fetch_array($result))
                    {
                        if($val['status']=='cancelled')
                            $icon="icon/red.png";
                        else if($val['status']=='waiting'||$val['status']=='ongoing')
                            $icon="icon/yellow.png";
                        else
                            $icon="icon/green.png";

                        echo "<tr>";
                        echo '<td class="title">'.$val['team1'].' vs '.$val['team2'].'</td>';
                        echo '<td class="score">'.$val['score'].'</td>';
                        echo '<td class="score">'.$val['date'].'</td>';
                        echo '<td class="status"><img src="'.$icon.'"></td>';
                        echo "</tr>";
                    }
                ?>
              </tbody>
            </table>
      </div>

    <!-- Start footer -->
    <footer>
      <div class="container">
        <p>&copy; E-ZSAT.PL 2018. All Rights Reserved.</p>
        <ul class="list-inline">
          <li class="list-inline-item">
            <a href="http://jakubbachorz.com/">Jakub Bachórz</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Jacek Gawron</a>
          </li>
          <li class="list-inline-item">
            <a href="#">Hubert Curzytek</a>
          </li>
        </ul>
      </div>
    </footer>

    <!-- Bootstrap and JavaScript -->
    <script src="../js/jquery/jquery.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../js/bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/jquery-easing/jquery.easing.min.js"></script>
    <script src="../js/script.js"></script>
  </body>

</html>
